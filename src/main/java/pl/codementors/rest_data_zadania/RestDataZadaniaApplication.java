package pl.codementors.rest_data_zadania;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestDataZadaniaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestDataZadaniaApplication.class, args);
	}

}
