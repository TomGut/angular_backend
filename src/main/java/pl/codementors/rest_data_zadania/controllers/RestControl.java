package pl.codementors.rest_data_zadania.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.codementors.rest_data_zadania.models.User;
import pl.codementors.rest_data_zadania.models.UserViews;
import pl.codementors.rest_data_zadania.repositories.UserRepo;

import java.util.List;

@RestController
public class RestControl {

    @Autowired UserRepo userRepo;

    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    public List<User> getUsers(){
        List<User> users = userRepo.findAll();
        return users;
    }

    //requestbody jest konieczny żeby z body mogłybyć wyciągniete parametry
    //save już nie mamy zdefiniowanego ale korzystamy bo mamy metodę w Crud zaimplementowaną
    @RequestMapping(value = {"user", "user/add"}, method = RequestMethod.POST)
    @JsonView(UserViews.UserId.class)
    public User addUser(@RequestBody User user){
        return userRepo.save(user);
    }

    @RequestMapping(value = "user/addAll", method = RequestMethod.POST)
    public List<User> addAllUsers(@RequestBody List<User> users){
        return (List<User>) userRepo.save(users);
    }

    @RequestMapping(value = "user/{userId}", method = RequestMethod.PUT)
    public HttpStatus apdateUser(@PathVariable("userId") Long id, @RequestBody User user){
        if(userRepo.findOne(id).getId() != null){
            User u = userRepo.findOne(id);
            u.setUsername(user.getUsername());
            u.setFirstname(user.getFirstname());
            u.setLastname(user.getLastname());
            u.setAge(user.getAge());
            userRepo.save(u);
            return HttpStatus.ACCEPTED;
        }
        return HttpStatus.NOT_FOUND;
    }

}
