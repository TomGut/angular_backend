package pl.codementors.rest_data_zadania.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.codementors.rest_data_zadania.models.User;

import java.util.List;

//ta adnotacja umożliwia uproszczenie procesu bez implementacji interfejsu, można go wstrzykiwać później
@Repository
public interface UserRepo extends CrudRepository<User, Long> {
    List<User> findAll();
    User findOne(Long id);
}
